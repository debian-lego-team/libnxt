Source: libnxt
Section: electronics
Priority: optional
Maintainer: Debian LEGO Team <debian-lego-team@lists.alioth.debian.org>
Uploaders:
 Petter Reinholdtsen <pere@debian.org>,
 Dominik george <nik@naturalnet.de>,
 Nicolas Schodet <nico@ni.fr.eu.org>,
Build-Depends:
 debhelper-compat (= 13),
 libusb-1.0-0-dev,
 meson,
 scdoc,
 pkgconf,
 gcc-arm-none-eabi
Standards-Version: 4.7.0
Homepage: https://git.ni.fr.eu.org/libnxt.git/about/
Vcs-Git: https://salsa.debian.org/debian-lego-team/libnxt.git
Vcs-Browser: https://salsa.debian.org/debian-lego-team/libnxt
Rules-Requires-Root: no

Package: libnxt
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: nxt-firmware
Description: utility library for talking to the LEGO Mindstorms NXT brick
 LibNXT is an utility library for talking to the LEGO Mindstorms NXT intelligent
 brick at a relatively low level. It currently does:
  * Handling USB communication and locating the NXT in the USB tree.
  * Interaction with the Atmel AT91SAM boot assistant.
  * Flashing of a firmware image to the NXT.
  * Execution of code directly in RAM.
 .
 The design of LibNXT is layered, meaning you can plug into it at any level of
 complexity or abstraction you desire, from the lowest level USB bulk
 bit-pushing interface, to an API exposing the SAM-BA commandset, right up to
 just calling nxt_firmware_flash() and having everything taken care of!
 .
 This package provides two binary utils:
  * fwflash is the program that uses LibNXT. As its name hints, its purpose is
    to take a NXT firmware image file and flash it to a connected NXT device.
  * fwexec is another utility, which takes a specially compiled firmware image,
    uploads it to the NXT's RAM, and executes it directly from there.
